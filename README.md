# PowerTools Analysis Scripts

A series of Python scripts to help analyzing data from Powerboard test results. Consists of the following components:

- `pbv3` module to parse, calibrate and convert the test results
- `tools` collection of helpful tools to manipulate and visualize test results
- `webreport` generation of a web reporting using results from the ITk production database
- `studies` a series of local dedicated studies

## Installation
The recommended method for using the reporting package is through a virtual Python environment. The following instructions target CentOS 7.

```shell
# Create and enter virtual environment at ~/venv/pwb (or other path of your choosing)
sudo yum install python36-virtualenv
python3.6 -m venv ~/venv/pwb
source ~/venv/pwb/bin/activate

# Install PB reporting package
git clone https://gitlab.cern.ch/berkeleylab/pbv3_test_adapter/reporting.git
pushd reporting
pip install -e .
popd
```

Don't forget to restore your virtual environment in new terminals before using the PB reporting package.
```shell
source ~/venv/pwb/bin/activate
```

### PetaLinux
There are problems installing numpy inside a virtual environment on PetaLinux. For this reason, all required packages should be installed system wide.

To find the reporting packages, the following should be run inside the reporting repository every session:
```sh
export PYTHONPATH=$(pwd):${PYTHONPATH}
```

## Tools
### pbv3_plot_monitor.py
Creates timeseries plots of all quantities monitored with the `pbv3_monitor` powertools program. 

### pbv3_convert.py
Converts test results from JSON files into a tabular CSV file. This is useful for large datasets, as loading the test results can take a long time. The resulting CSV file can be directly loaded with the main `pbv3.tests.load_testresults` function.

## Web Interfaces
The reporting package ships with several web interfaces based on the Dash framework.

### Powerboard QC GUI

The main code for the Powerboard QC GUI is located inside `webreport/pwbDashGUI`. The web app can be started with the following command:
```sh
webapps/PWBTestingGUI.py -c pwbgui.json
```

The `pwbgui.json` is an optional configuration file for the GUI. The documantion for all the settings is available in the `webreport.pwbDashGUI.config.Config` docstring. An example, used for the reception setup, is as follows:
```json
{
    "version": 0.1,
    "username": "admin",
    "password": "secret",
    "fwdir": "/media/sd-mmcblk0p1",
    "panels": [
        {
            "name"      : "Powerboard Reception Testing",
            "runner"    : "PowertoolsRunner",
            "equipconf" : "/etc/powertools/equip_testbench.json",
            "datadir"   : "/data/testruns"
        }
    ]
}
```
