These scripts use the library `itkdb` to interact with the ITk Production Database.

To run them without having to enter your two access codes each time a script is run, you must set the following environment variables:

```shell
export ITKDB_ACCESS_CODE1 = accesscode1
export ITKDB_ACCESS_CODE2 = accesscode2

#with accesscode1 and accesscode2 replaced by your two database passwords.
```
