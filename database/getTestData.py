#!/usr/bin/env python3
    
import os
import itkdb
import json
import argparse

tests={
    "LV_ENABLE":
        ["LVENABLE",
         "VOUT",
         "IIN",
         "IOUT",
         "VIN"],
    "HV_ENABLE":
        ["HVENABLE",
         "HVIIN",
         "HVVIN"],
    "DCDCEFFICIENCY":
        ["IOUT",
         "EFFICIENCY",
         "AMACPTAT",
         "AMACVDCDC",
         "IINOFFSET",
         "VIN",
         "IIN"],
    "HVSENSE":
        ["AMACGAIN0",
         "AMACGAIN1",
         "AMACGAIN2",
         "AMACGAIN4",
         "AMACGAIN8",
         "HVI",
         "HVV",
         "ILEAK"],
    "STATUS":
        ["VIN",
         "IIN",
         "VOUT",
         "IOUT",
         "AMACVDCDC",
         "AMACVDDLR",
         "AAMCDCDCIN",
         "AMACVDDREG",
         "AMAC900BG",
         "AMAC600BG",
         "AMACCAL",
         "AMACNTCPB",
         "AMACCUR10V",
         "AMACCUR1V",
         "AMACHVRET",
         "AMACPTAT",
         "HVVIN",
         "HVIIN"],
    "BER":
        ["RELIABILITY"],
    "VIN":
        ["VINSET",
         "VIN",
         "IIN",
         "AMACDCDCIN"],
    "OF":
        ["OF",
         "VIN",
         "IIN",
         "linPOLV"],
    "CUR10V":
        ["DCDCiP",
         "DCDCiN",
         "AMACCUR10V",
         "DCDCiOffset"],
    "CUR1V":
        ["DCDCiP",
         "DCDCiN",
         "AMACCUR10V",
         "DCDCiOffset"]
}

def getResults(version, ids, runs, resultDict, no_cache):
    for boards in version:
        if no_cache:
            testsByComp = c.get("listTestRunsByComponent", json = {'component':boards['code'], 'testType':list(tests.keys())}, headers = {'Cache-Control':'no-cache'})
        else:
            testsByComp = c.get("listTestRunsByComponent", json = {'component':boards['code'], 'testType':list(tests.keys())})

        for testTypes in testsByComp:
            for types in list(tests.keys()):
                if(testTypes["testType"]["code"] == types):
                    ids[types].append(testTypes["id"])

    for testTypes in ids:
        for runId in ids[testTypes]:
            if no_cache:
                runs[testTypes].append(c.get("getTestRun", json = {'testRun':runId}, headers = {'Cache-Control':'no-cache'}))
            else:
                runs[testTypes].append(c.get("getTestRun", json = {'testRun':runId}))

    for testTypes in list(tests.keys()):
        for testrun in runs[testTypes]:
            for results in testrun["results"]:
                for subtests in tests[testTypes]:
                    if(results["code"]==subtests):
                        resultDict[testTypes][subtests].append(results["value"])

if __name__ == "__main__": 
    parser = argparse.ArgumentParser(description = "Gets powerboard data for HTML display.")
    parser.add_argument("saveLocation", help="Location in which to store json files.")
    parser.add_argument("-nc","--no_cache", action='store_true', help="Run the script without checking the cache.")
        
    args = parser.parse_args()
        
    global u
    global c

    if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
      accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
      accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
    else:
      accessCode1 = getpass.getpass("AccessCode1: ")
      accessCode2 = getpass.getpass("AccessCode2: ")

    u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
    c = itkdb.Client(user = u, expires_after=dict(days=1))
    c.user.authenticate()

    testIDs = {}
    testRuns = {}
    testResults = {}
        
    testIDs_v3a = {}
    testRuns_v3a = {}
    testResults_v3a = {}
        
    testIDs_v3b = {}
    testRuns_v3b = {}
    testResults_v3b = {}
        
    for test in list(tests.keys()):
        testIDs[test] = []
        testRuns[test] = []
        testResults[test] = {}
        for subtests in tests[test]:
            testResults[test][subtests] = []
        
    for test in list(tests.keys()):
        testIDs_v3a[test] = []
        testRuns_v3a[test] = []
        testResults_v3a[test] = {}
        for subtests in tests[test]:
            testResults_v3a[test][subtests] = []
        
    for test in list(tests.keys()):
        testIDs_v3b[test] = []
        testRuns_v3b[test] = []
        testResults_v3b[test] = {}
        for subtests in tests[test]:
            testResults_v3b[test][subtests] = []

    if args.no_cache:
        pwb = c.get("listComponents", json = {'project':'S', 'subproject':['SB'], 'componentType':['PWB'], 'type':['B3']}, headers = {'Cache-Control':'no-cache'})
    else:
        pwb = c.get("listComponents", json = {'project':'S', 'subproject':['SB'], 'componentType':['PWB'], 'type':['B3']})

    pwb_allv = []
    pwb_v3a = []
    pwb_v3b = []
        
    for items in pwb:
        pwb_allv.append(items)
        for props in items['properties']:
            if props['code'] == 'VERSION' and (props['value'] == 'v3.0' or props['value'] == 'v3.0a' or props['value'] == '0' or props['value']=='1'):
                pwb_v3a.append(items)
            if props['code'] == 'VERSION' and (props['value'] == 'v3.0b' or props['value']=='2'):
                pwb_v3b.append(items)
        
    getResults(pwb_allv, testIDs, testRuns, testResults, args.no_cache)
    getResults(pwb_v3a, testIDs_v3a, testRuns_v3a, testResults_v3a, args.no_cache)
    getResults(pwb_v3b, testIDs_v3b, testRuns_v3b, testResults_v3b, args.no_cache)
        
    json_all_v = json.dumps(testResults)
    f = open(args.saveLocation + "/testResults.json", "w")
    f.write(json_all_v)
    f.close()
        
    json_v3a = json.dumps(testResults_v3a)
    g = open(args.saveLocation + "/testResults_v3a.json", "w")
    g.write(json_v3a)
    g.close()
        
    json_v3b = json.dumps(testResults_v3b)
    h = open(args.saveLocation + "/testResults_v3b.json", "w")
    h.write(json_v3b)
    h.close()
