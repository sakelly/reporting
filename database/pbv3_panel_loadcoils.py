#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import transitions

def soldercoils(client, panelid, currentLocation):
    """
    Assemble coils onto all Powerboards on a panel
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     currentLocation (str): filter for currentLocation of random components
    """

    print('Solder coils on panel {}'.format(panelid))
    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client)

    # Get necessary amount of random components
    nflex=len(panel.children['PB_FLEX'])
    print('Found {} flexes on panel.'.format(nflex))
    coils=map(lambda code: pwbdb.Component(code, client=client),
                  pwbdbtools.get_random_components(client, 'SG', 'PWB_COIL', 'COILV3A', currentLocation, n=nflex)
                  )

    # Loop over children
    for flex in panel.children['PB_FLEX'].values():
        powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
        coil=next(coils)
        print('Assemble coil {} to powerboard {}'.format(coil.code,powerboard.code))
        powerboard.assemble(coil)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid", help="Panel identifier (VBBXXXX).")
    parser.add_argument("-c","--currentLocation", default = 'LBNL_STRIP_POWERBOARDS', help="Filter random coilss by current location.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    soldercoils(c, args.panelid, args.currentLocation)
