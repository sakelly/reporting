#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import transitions

def loadhvmux(client, panelid, currentLocation):
    """
    Assemble HVmux onto all Powerboards on a panel
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     currentLocation (str): filter for currentLocation of random components
    """

    print('Glue HVmux on panel {}'.format(panelid))
    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client)
    if panel.panelType!='PWB_CARRIER':
        raise Exception('Powerboards must be on a Carrier Card for chip gluing. ')

    # Get necessary amount of random components
    npwb=len(panel.children['PWB'])
    print('Found {} Powerboards on panel.'.format(npwb))
    hvmuxs=map(lambda code: pwbdb.Component(code, client=client),
               pwbdbtools.get_random_components(client, 'SG', 'HVMUX', 'HVMUX1', currentLocation, n=npwb)
    )

    # Loop over children
    for pwb in panel.children['PWB'].values():
        if len(pwb.children['HVMUX'])>0:
            print('Powerboard {} already has a HVmux. Skip...'.format(pwb.code))
            continue
        hvmux=next(hvmuxs)
        print('Glue HVmux {} to Powerboard {}'.format(hvmux.code,pwb.code))
        pwb.assemble(hvmux)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid", help="Panel identifier (VBBXXXX).")
    parser.add_argument("-c","--currentLocation", default = 'LBNL_STRIP_POWERBOARDS', help="Filter random HVMUXs by current location.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    loadhvmux(c, args.panelid, args.currentLocation)
