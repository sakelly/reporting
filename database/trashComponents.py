#!/usr/bin/env python3

import os
import argparse
import itkdb
import getpass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Trashes the specified number of a certain component and component type.")

    parser.add_argument("component", help="Code of component.")
    parser.add_argument("type", help="Code for component subtype.")
    parser.add_argument("num_of_components", type = int, help="Number of components you wish to trash.")
    parser.add_argument("-nc", "--no_cache", action = 'store_true')

    args = parser.parse_args()

    global u
    global c
  
    if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
      accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
      accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
    else:
      accessCode1 = getpass.getpass("AccessCode1: ")
      accessCode2 = getpass.getpass("AccessCode2: ")
  
    u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
    c = itkdb.Client(user = u, expires_after=dict(days=1))
    c.user.authenticate()

    n = 0
    page = 0
    pageSize = 200
    num_comps = 0
    while n < args.num_of_components:
        if args.no_cache:
            comp_list = c.get("listComponents", json = {'project':'S', 'componentType':args.component, 'type':args.type, 'institution':'LBL', 'assembled': False, 'pageInfo':{'pageSize':pageSize, 'pageIndex':page}}, headers = {'Cache-Control': 'no-cache'})
        else:
            comp_list = c.get("listComponents", json = {'project':'S', 'componentType':args.component, 'type':args.type, 'institution':'LBL', 'assembled': False, 'pageInfo':{'pageSize':pageSize, 'pageIndex':page}})

        for component in comp_list:
            num_comps = num_comps + 1
            if n == args.num_of_components:
                break

            if args.no_cache:
                get_comp = c.get("getComponent", json = {'component':component['code']}, headers = {'Cache-Control': 'no-cache'})
            else:
                get_comp = c.get("getComponent", json = {'component':component['code']})

            if component['trashed']==False:
                c.post("setComponentTrashed", json = {'component':component['code'], 'trashed':True})
                print("Trashed: " + component['code'])
                n = n + 1

        if num_comps < pageSize:
            if n < args.num_of_components:
                print("Attempting to trash more unassembled components than are currently registered. Consider running with the --no_cache argument if you are certain this is not the case. This checks the most up to date version of the database.")
            break
        page = page + 1
