# ITk Production Database time study comparison scripts between the two libraries itkdb and production\_database\_scripts

## Dependencies
- numpy
- matplotlib
- itkdb
- production\_database\_scripts

## Database Authorization
Running these scripts will prompt you to input both of your database passwords twice, once for each library.

To avoid being prompted to input your passwords, you may do the following:
For `itkdb` set the following two environment variables by running
```shell
export ITKDB_ACCESS_CODE1 = accesscode1
export ITKDB_ACCESS_CODE2 = accesscode2

with accesscode1 and accesscode2 replaced by your database passwords.
```

For `production_database_scripts` you need to run `get_token.py`, included as part of that package. Copy and paste what is output after you enter your passwords, including `export`.

## Running the scripts
Create a virtual environment to install the above dependencies.
`numpy`, `matplotlib`, and `itkdb` can all be installed with `pip install`

To install `production_database_scripts` as a module, create a virtual environment, activate it, and run the following:

```shell
git clone https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts.git
cd production_database_scripts
pip install -e .
```
The following scripts generate json files in which the test results are stored:
- assembleComponents.py
- getPWBFromAMACeFuse.py
- getComponent\_serial\_vs\_code.py
- getTestData.py

assembleComponents.py gathers all available components to fully assemble 50 powerboards (minus the AMACs). This looped through 10 times.

getPWBFromAMACeFuse.py takes a set of AMAC Hex eFuse IDs and gets the powerboard numbers for which those AMACs are the children. The 8 eFuse IDs that it looks up are hardcoded. This is looped through 50 times.

getTestData.py gets all test data for all powerboards

getComponent\_serial\_vs\_code.py compares the difference in the time it takes to perform getComponent if you use the serial number vs the component code.

The test results are saved in the `data` folder.

All scripts, except for getComponent\_serial\_vs\_code.py, do not require arguments to run.
For getComponent\_serial\_vs\_code.py, you must pass it the database codes for the component type and component subtype as arguments.

Due to a database error that has not yet been solved, the scripts will occassionally crash. As getTestData.py is rather long, it tends to crash more often than the other scripts. In order to record any data, without crashing, this script only loops through once. In order to get more data points, you have to rerun the script multiple times. Each time the data will be appended to the corresponding json file. All other scripts only need to be run once.

The following scripts plot the resultant json file data:
- plotAssembleComponents.py
- plotGetPWBFromAMACeFuse.py
- plotGetComponent\_serial\_vs\_code.py
- plotGetTestData.py

None of these scripts require arguments to run and the resultant png files will also be stored in the `data` folder.
