#!/usr/bin/env python3

if __name__ == '__main__':
    from __path__ import updatePath
    updatePath()
    
import os
import time
import json
import itk_pdb.dbAccess as dbAccess
import argparse
import itkdb
from itk_pdb.dbAccess import ITkPDSession
import getpass

tests={
    "LV_ENABLE":
        ["LVENABLE",
         "VOUT",
         "IIN",
         "IOUT",
         "VIN"],
    "HV_ENABLE":
        ["HVENABLE",
         "HVIIN",
         "HVVIN"],
    "DCDCEFFICIENCY":
        ["IOUT",
         "EFFICIENCY",
         "AMACPTAT",
         "AMACVDCDC",
         "IINOFFSET",
         "VIN",
         "IIN"],
    "HVSENSE":
        ["AMACGAIN0",
         "AMACGAIN1",
         "AMACGAIN2",
         "AMACGAIN4",
         "AMACGAIN8",
         "HVI",
         "HVV",
         "ILEAK"]
#    "STATUS":
#        ["VIN",
#         "IIN",
#         "VOUT",
#         "IOUT",
#         "AMACVDCDC",
#         "AMACVDDLR",
#         "AAMCDCDCIN",
#         "AMACVDDREG",
#         "AMAC900BG",
#         "AMAC600BG",
#         "AMACCAL",
#         "AMACNTCPB",
#         "AMACCUR10V",
#         "AMACCUR1V",
#         "AMACHVRET",
#         "AMACPTAT",
#         "HVVIN",
#         "HVIIN"],
#    "BER":
#        ["RELIABILITY"],
#    "VIN":
#        ["VINSET",
#         "VIN",
#         "IIN",
#         "AMACDCDCIN"],
#    "OF":
#        ["OF",
#         "VIN",
#         "IIN",
#         "linPOLV"],
#    "CUR10V":
#        ["DCDCiP",
#         "DCDCiN",
#         "AMACCUR10V",
#         "DCDCiOffset"],
#    "CUR1V":
#        ["DCDCiP",
#         "DCDCiN",
#         "AMACCUR10V",
#         "DCDCiOffset"]
}

def getResults(version, ids, runs, resultDict, lib):
    for boards in version:
        #List all test runs for given powerboard
        #production_database_scripts
        if lib=='p':
            testsByComp = session.doSomething(action = "listTestRunsByComponent", data = {'component':boards['code'], 'testType':list(tests.keys())}, method = 'GET')
        #itkdb with caching
        if lib=='c':
            testsByComp = c.get("listTestRunsByComponent", json = {'component':boards['code'], 'testType':list(tests.keys())})
        #itkdb without caching
        if lib=='i':
            testsByComp = c.get("listTestRunsByComponent", json = {'component':boards['code'], 'testType':list(tests.keys())}, headers={'Cache-Control':'no-cache'})

        #Separate out test result IDs based on test type
        for testTypes in testsByComp:
            for types in list(tests.keys()):
                if(testTypes["testType"]["code"] == types):
                    ids[types].append(testTypes["id"])

    #Get test results
    for testTypes in ids:
        for runId in ids[testTypes]:
            #production_database_scripts
            if lib=='p':
                runs[testTypes].append(session.doSomething(action = "getTestRun", data = {'testRun':runId}, method = 'GET'))
            #itkdb with caching
            if lib=='c':
                runs[testTypes].append(c.get("getTestRun", json = {'testRun':runId}))
            #itkdb without caching
            if lib=='i':
                runs[testTypes].append(c.get("getTestRun", json = {'testRun':runId}, headers={'Cache-Control':'no-cache'}))

    for testTypes in list(tests.keys()):
        for testrun in runs[testTypes]:
            for results in testrun["results"]:
                for subtests in tests[testTypes]:
                    if(results["code"]==subtests):
                        resultDict[testTypes][subtests].append(results["value"])

def runScript(lib):
    runTimes = []
    for i in range (0,1):
        now = time.time()
    
        testIDs = {}
        testRuns = {}
        testResults = {}
            
        testIDs_v3a = {}
        testRuns_v3a = {}
        testResults_v3a = {}
            
        testIDs_v3b = {}
        testRuns_v3b = {}
        testResults_v3b = {}
            
        #Generate dictionaries from test keys given above by tests
        for test in list(tests.keys()):
            testIDs[test] = []
            testRuns[test] = []
            testResults[test] = {}
            for subtests in tests[test]:
                testResults[test][subtests] = []
            
        for test in list(tests.keys()):
            testIDs_v3a[test] = []
            testRuns_v3a[test] = []
            testResults_v3a[test] = {}
            for subtests in tests[test]:
                testResults_v3a[test][subtests] = []
            
        for test in list(tests.keys()):
            testIDs_v3b[test] = []
            testRuns_v3b[test] = []
            testResults_v3b[test] = {}
            for subtests in tests[test]:
                testResults_v3b[test][subtests] = []

        #production_database_scripts
        if lib=='p':
            pwb = session.doSomething(action = "listComponents", data = {'project':'S', 'subproject':['SB'], 'componentType':['PWB'], 'type':['B3']}, method = 'GET')
        #itkdb with caching
        if lib=='c':            
            pwb = c.get("listComponents", json = {'project':'S', 'subproject':['SB'], 'componentType':['PWB'], 'type':['B3']})
        #itkdb without caching
        if lib=='i':
            pwb = c.get("listComponents", json = {'project':'S', 'subproject':['SB'], 'componentType':['PWB'], 'type':['B3']}, headers={'Cache-Control':'no-cache'})

        pwb_allv = []
        pwb_v3a = []
        pwb_v3b = []

        #Separate out powerboard versions    
        for items in pwb:
            pwb_allv.append(items)
            for props in items['properties']:
                if props['code'] == 'VERSION' and (props['value'] == 'v3.0' or props['value'] == 'v3.0a' or props['value'] == '0' or props['value']=='1'):
                    pwb_v3a.append(items)
                if props['code'] == 'VERSION' and (props['value'] == 'v3.0b' or props['value']=='2'):
                    pwb_v3b.append(items)
        
        getResults(pwb_allv, testIDs, testRuns, testResults, lib)
        getResults(pwb_v3a, testIDs_v3a, testRuns_v3a, testResults_v3a, lib)
        getResults(pwb_v3b, testIDs_v3b, testRuns_v3b, testResults_v3b, lib)

        runTimes.append(time.time()-now)

    return runTimes

if __name__ == "__main__": 

    if os.environ.get('ITK_DB_AUTH'):
        dbAccess.token = os.getenv('ITK_DB_AUTH')
        
    global session
    session = ITkPDSession()
    session.authenticate()

    if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
        accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
        accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
    else:
        accessCode1 = getpass.getpass("AccessCode1: ")
        accessCode2 = getpass.getpass("AccessCode2: ")
  
    u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
    c = itkdb.Client(user = u, expires_after=dict(days=1))
    c.user.authenticate()
    
    pds = runScript('p')
    ITKDB = runScript('i')
    ITKDBCa = runScript('c')

    if os.path.exists('data/getTestData.json'):
        with open('data/getTestData.json', 'r') as f:
            data = json.load(f)
            data['pds'] = data['pds'] + pds
            data['ITKDB'] = data['ITKDB'] + ITKDB
            data['ITKDBCa'] = data['ITKDBCa'] + ITKDBCa
    else:
        data = {}
        data['pds'] = pds
        data['ITKDB'] = ITKDB
        data['ITKDBCa'] = ITKDBCa

    with open('data/getTestData.json', 'w+') as f:
        json.dump(data, f)
