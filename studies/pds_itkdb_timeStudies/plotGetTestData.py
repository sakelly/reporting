#!/usr/bin/env python3

import os
import json
import numpy as np
import matplotlib.pyplot as plt

#with open('itkdbHist/getTestData/pds.txt', 'r') as f:
#    pds = [float(time.rstrip()) for time in f.readlines()]

#with open('data/getTestData.csv', 'r') as f:
#    read = list(reader(f))
#    pds = [float(i) for i in read[0]]
#    ITKDB = [float(i) for i in read[1]]
#    ITKDBCa = [float(i) for i in read[2]]

with open('data/getTestData.json', 'r') as f:
    data = json.load(f)

pds = data['pds']
ITKDB = data['ITKDB']
ITKDBCa = data['ITKDBCa']

bins = np.histogram(np.hstack((pds, ITKDB, ITKDBCa)), bins = 'auto')[1]
plt.hist(x=pds, bins=bins, alpha=0.5, label='production_database_scripts: Mean ' + str(round(sum(pds)/len(pds), 2)))
plt.hist(x=ITKDB, bins=bins, alpha=0.5, label='itkdb: Mean ' + str(round(sum(ITKDB)/len(ITKDB), 2)))
plt.hist(x=ITKDBCa, bins=bins, alpha=0.5, label='itkdb Cached: Mean ' + str(round(sum(ITKDBCa)/len(ITKDBCa), 2)))
plt.title('Total Time of getTestData.py')
plt.xlabel('Time [s]')
plt.legend()
plt.show()
plt.savefig("data/getTestData.png")
plt.close()
