#!/usr/bin/env python

import pkg_resources
import pandas as pd
import jinja2
import shutil
import datetime
import os, sys

from database import pwbdbtools
from database import pwbdbpandas

if len(sys.argv)!=2:
    print('usage: {} outdir/'.format(sys.argv[0]))
    sys.exit(1)

#
# Checks on input
output_dir=sys.argv[1]

if not os.path.isdir(output_dir):
    print('ERROR: Output directory does not exist.')
    sys.exit(1)

#
#
c = pwbdbtools.get_db_client()

#
# Powerboards
df=pwbdbpandas.listComponents(c, {'project':'S', 'subproject':'SB', 'componentType':'PWB', 'type':'B3'})
df=df[(df.state=='ready')]

stages=pwbdbpandas.getComponentTypeStages(c, 'PWB')

# Powerboard versions
r=c.get('getComponentTypeByCode',json={'project':'S', 'code':'PWB'})
versionCodeTable={ct['code']:ct['value'] for ct in next(filter(lambda p: p['code']=='VERSION', r['properties']))['codeTable']}

# Count all powerboards
counts=df.groupby('VERSION').code.count()
df_pb_versions=pd.DataFrame(data=counts.index.map(versionCodeTable).rename('name'), index=counts.index)
df_pb_versions['count']=counts

data_pb=[]
for index,pbtype in df_pb_versions.iterrows():
    data_pb.append({'title':pbtype['name'],'count':pbtype['count']})
data_pb.append({'title':'Total:','count':df_pb_versions['count'].sum()})

# Count local Powerboards
cnt=df[(df.currentLocation=='LBNL_STRIP_POWERBOARDS')&(df.shipmentDestination.isna())].groupby(['VERSION','currentStage']).code.count().rename('count').reset_index()
stage_counts=stages.merge(cnt,left_on='code',right_on='currentStage',how='outer')
stage_counts['VERSION']=stage_counts['VERSION'].map(versionCodeTable)

stage_counts=stage_counts[['name','count','VERSION']]
stage_counts=stage_counts.pivot(columns='name',index='VERSION').fillna(0)
stage_counts.columns=map(lambda c: c[1],stage_counts.columns)

stage_counts=stage_counts[~stage_counts.index.isna()]
stage_counts.loc['Total:']=stage_counts.sum()
stage_counts=stage_counts[stages['name']].astype(int)

#
# Powerboards at locations

# Add special stage for "in shipment"
stages=stages.append({'code':'SHIPPING', 'name':'In Shipment', 'order':4.5}, ignore_index=True)
ship_stages=stages[stages.code.isin(['SMD_LOAD', 'MAN_LOAD', 'BONDED', 'THERMAL', 'BURN_IN', 'SHIPPING','MODULE_RCP','LOADED', 'HYBBURN'])].sort_values('order')

# All powerboards at location
cnt=df[df.currentLocation!='LBNL_STRIP_POWERBOARDS'].groupby(['VERSION','currentLocation','currentStage']).code.count().rename('count').reset_index()

# All powerboards being shipped to location
cnt_ship=df[(df.currentLocation=='LBNL_STRIP_POWERBOARDS')&(~df.shipmentDestination.isna())].groupby(['VERSION','shipmentDestination']).code.count().reset_index().rename(columns={'code':'count','shipmentDestination':'currentLocation'})
cnt_ship['currentStage']='SHIPPING'
cnt=pd.concat([cnt,cnt_ship])

# Rotat table into columns=stages at location, rows=location.
# MultiIndex by VERSION and location
ship_counts=ship_stages.merge(cnt,left_on='code',right_on='currentStage',how='left').sort_values('order')

ship_counts['VERSION']=ship_counts['VERSION'].map(versionCodeTable)
ship_counts=ship_counts[['VERSION','currentLocation','name','count']]
ship_counts=ship_counts.pivot(columns=['name'],index=['VERSION','currentLocation'])
ship_counts.columns=map(lambda c: c[1],ship_counts.columns)

ship_counts=ship_counts[ship_stages['name']].fillna(0).astype(int)

#
# AMAC
df_amac=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'AMAC', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})

# Get available component types
r=c.get('getComponentTypeByCode', json={'project':'S','code':'AMAC'})
df_amactype=pd.DataFrame.from_records(r['types'], columns=['code','name','existing'])
df_amactype=df_amactype[df_amactype.existing]
df_amactype=df_amactype.set_index('code')

# Counts
df_amactype['count']=df_amac.groupby('type').code.count()
df_amactype['count']=df_amactype['count'].fillna(0).astype(int)

data_amac=[]
for index,amactype in df_amactype.sort_values('name').iterrows():
    data_amac.append({'title':amactype['name'],'count':amactype['count']})
data_amac.append({'title':'Total:','count':df_amactype['count'].sum()})

#
# bPOL
df_bpol=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'BPOL12V', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})

# Get available component types
r=c.get('getComponentTypeByCode', json={'project':'S','code':'BPOL12V'})
df_bpoltype=pd.DataFrame.from_records(r['types'], columns=['code','name','existing'])
df_bpoltype=df_bpoltype[df_bpoltype.existing]
df_bpoltype=df_bpoltype.set_index('code')

# Counts
df_bpoltype['count']=df_bpol.groupby('type').code.count()
df_bpoltype['count']=df_bpoltype['count'].fillna(0).astype(int)

data_bpol=[]
for index,bpoltype in df_bpoltype.sort_values('name').iterrows():
    data_bpol.append({'title':bpoltype['name'],'count':bpoltype['count']})
data_bpol.append({'title':'Total:','count':df_bpoltype['count'].sum()})

#
# SMD Components
df_shieldbox=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'PWB_SHIELDBOX', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})
N_shieldbox=len(df_shieldbox.index)

df_coil=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'PWB_COIL', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})
N_coil=len(df_coil.index)

df_hvmux=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'HVMUX', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})
N_hvmux=len(df_hvmux.index)

df_linpol=pwbdbpandas.listComponents(c, {'project':'S', 'componentType':'PWB_LINPOL', 'currentLocation':'LBNL_STRIP_POWERBOARDS','assembled':False})
N_linpol=len(df_linpol.index)

#
# Render
template_dir=pkg_resources.resource_filename(__name__,'template')

file_loader = jinja2.FileSystemLoader(template_dir)
env = jinja2.Environment(loader=file_loader)

t=env.get_template('index.html')
output=t.render(
    stages=stage_counts,
    ships=ship_counts,

    allpbs=data_pb,
    amacs=data_amac,
    bpols=data_bpol,
    
    N_shieldbox=N_shieldbox,
    N_coil=N_coil,
    N_hvmux=N_hvmux,
    N_linpol=N_linpol,

    date_generated=datetime.datetime.strftime(datetime.datetime.now(),'%c')
    )

# Outputs
with open('{output_dir}/index.html'.format(output_dir=output_dir), 'w') as htmlfile:
    htmlfile.write(output)

if os.path.exists(output_dir+'/static'):
    shutil.rmtree(output_dir+'/static')
shutil.copytree(template_dir+'/static',output_dir+'/static')
