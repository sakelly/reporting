if(!window.dash_clientside) {window.dash_clientside = {};}

function createGraph(data, title, xname,xtitle,xrange,xdticks, yname,ytitle,yrange,ydticks)
{
    let fig = {'data': [],
               'layout': {
                   'title': {'text': title},
                   'width': 600,
                   'height': 500,
                   'showlegend': true,                   
                   'template': '...',
                   'xaxis': {'title': {'text': xtitle}, 'range': xrange, 'dtick': xdticks},
                   'yaxis': {'title': {'text': ytitle}, 'range': yrange, 'dtick': ydticks}}
              };

    if(data !== undefined)
    {
	for(let i=0; i<data.length; i++)
	{
            let trace={'mode': 'markers', 'name': i, 'type': 'scatter'};
            trace['x']=(data[i][xname].length>0)?data[i][xname]:[null];
            trace['y']=(data[i][yname].length>0)?data[i][yname]:[null];
            fig['data'][i]=trace;
	}
    }

    return fig;
}

window.dash_clientside.clientside = {

    // Stores setting information for a panel, such that it can
    // be restored on tab switch.
    //
    // The arguments are the values of elements that should be
    // stored, followed by the current `panelstorage` data
    // and the current `tab`.
    //
    // The returned `panelstorage` has value at key `tab` updated
    // to contain a list of arguments[0:-2].
    storePanelInfo: function() {
        var panelstorage=arguments[arguments.length-2];
        var tab=arguments[arguments.length-1];

        // Allocate objects if needed
        if(panelstorage === undefined)
        { panelstorage={}; }

        //panelstorage[tab]=Array.prototype.slice.call(arguments,0,arguments.length-2);
        panelstorage[tab]=[...arguments].slice(0,arguments.length-2);
        
        return panelstorage;
    },

    // Restores the panel setting from storage by returning the
    // stored values in `panelstorage` at `tab`.
    //
    // If called as part of an update from `dbresult` store, then
    // the stored value is augmented with the contained result.
    //
    // If a `panelstorage` or the corresponding `tab` values
    // is not defined, then default values (hardcoded!) are
    // returned.
    restorePanelInfo: function(tab, dbresult, panelstorage) {
	//
	// Build up stored result

	// Blank default if nothing is stored
	result=[
            "","","",
            [],[],[],[],[],[],[],[],[],[],
            "","","","","","","","","","",
            0
        ]
	
	// Check for existing store
	if(panelstorage !== undefined && panelstorage[tab] !== undefined)
        { result=panelstorage[tab]; }

	// There is an update from the database
	for(let c=0; c<dash_clientside.callback_context.triggered.length; c++)
	{
	    let trigger=dash_clientside.callback_context.triggered[c];
	    if(trigger.prop_id=="panel-dbresult.data")
	    {
		for(let i=0; i<10; i++)
		{
		    result[1]=dbresult['version']
		    result[2]=dbresult['batch']
		    result[ 3+i]=(dbresult['pbEnable'][i]===true)?[i]:[];
                    result[13+i]=dbresult['pbNumber'][i];
		}
	    }
	}

        return result;
    },

    //
    // Run status updates
    //

    updateTabColors: function(panelStatus) {
        let tabColors = [];
       
        if(panelStatus === undefined)
        {
            for(let i=0; i<10; i++)
            {
                tabColors.push({'background-color':'black'});
                tabColors.push({'background-color':'black'});
            }
            return tabColors;
        }

        for(let i=0; i<panelStatus.length; i++)
        {
            if (panelStatus[i])
            {
                tabColors.push({'background-color':'green'});
                tabColors.push({'background-color':'green'});
            }
            else
            {
                tabColors.push({'background-color':''});
                tabColors.push({'background-color':''});
            }
        }

        return tabColors;
    },

    //
    // Data updates
    //
    updateTIOtable: function(data) {
        return [data, data];
    },

    updateLVIVgraph: function(data) {
        return createGraph(data,
                           "Low Voltage Scan",
                           "VIN",        "Input Voltage [V]"     , [0,11]  , 1,
                           "AMACDCDCIN", "Input Voltage [counts]", [0,1024], 128
                          );
    },

    updateAMSlopegraph: function(data) {
        return createGraph(data,
                           "AM Response",
                           "CALIN"  , "CAL Input [V]"    , [0,1]   , 0.1,
                           "AMACCAL", "AMAC CAL [counts]", [0,1024], 128
                          );
    },

    updateDACgraph: function(data) {
        return [
            createGraph((data!==undefined)?data["rshx"]:undefined,
                        "Shunt,x",
                        "DAC"      , "DAC Setting", [0,256], 32,
                        "External" , "DAC Output [V]" , [0,1.2], 0.1
                       ),
            createGraph((data!==undefined)?data["rshy"]:undefined,
                        "Shunt,y",
                        "DAC"      , "DAC Setting", [0,256], 32,
                        "External" , "DAC Output [V]" , [0,1.2], 0.1
                       ),
            createGraph((data!==undefined)?data["rcax"]:undefined,
                        "Cal,x",
                        "DAC"      , "DAC Setting", [0,256], 32,
                        "External" , "DAC Output [V]" , [0,1.2], 0.1
                       ),
            createGraph((data!==undefined)?data["rcay"]:undefined,
                        "Cal,y",
                        "DAC"      , "DAC Setting", [0,256], 32,
                        "External" , "DAC Output [V]" , [0,1.2], 0.1
                       )
        ];
    },

    updateDCDCgraph: function(data) {
        return [
            createGraph(data,
                        "DC/DC Load Scan",
                        "IOUT"      , "Load Current [A]", [0,4], 0.2,
                        "EFFICIENCY", "DC/DC Efficiency", [0,1], 0.1
                       ),
            createGraph(data,
                        "DC/DC Load Scan",
                        "IOUT"    , "Load Current [A]"          , [0,4]   , 0.2,
                        "AMACPTAT", "DC/DC Temperature [counts]", [0,1024], 128
                       ),
            createGraph(data,
                        "DC/DC Load Scan",
                        "IOUT"      , "Load Current [A]", [0,4]   , 0.2,
                        "AMACCUR10V", "Current [counts]", [0,1024], 128
                       ),
            createGraph(data,
                        "DC/DC Load Scan",
                        "IOUT"     , "Load Current [A]", [0,4]   , 0.2,
                        "AMACCUR1V", "Current [counts]", [0,1024], 128
                       )
        ];
    },
    
    updateHVretgraph: function(data) {
        return [
            createGraph(data,
                        "Gain Setting 0x0",
                        "HVIOUT"   , "Sensor Current [A]"     , [0,2.5e-6], 0.25e-6,
                        "AMACGAIN0", "Sensor Current [counts]", [0,1024], 128
                       ),
            createGraph(data,
                        "Gain Setting 0x1",
                        "HVIOUT"   , "Sensor Current [A]"     , [0,25e-6], 2.5e-6,
                        "AMACGAIN1", "Sensor Current [counts]", [0,1024] , 128
                       ),
            createGraph(data,
                        "Gain Setting 0x2",
                        "HVIOUT"   , "Sensor Current [A]"     , [0,0.25e-3], 0.025e-3,
                        "AMACGAIN2", "Sensor Current [counts]", [0,1024]  , 128
                       ),
            createGraph(data,
                        "Gain Setting 0x4",
                        "HVIOUT"   , "Sensor Current [A]"     , [0,1e-3], 0.1e-3,
                        "AMACGAIN4", "Sensor Current [counts]", [0,1024], 128
                       )
        ];
    }
}
