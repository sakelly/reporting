import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash_table.Format import Format, Scheme, Sign, Symbol
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate

import plotly.graph_objects as go
from time import time
import codecs
import pickle
import datetime
import pkg_resources

import pandas as pd

from webreport.pwbDashCommon import template

from . import panel

from database import pwbdb

import itkdb

def pad_dataframe_pbs(df):
    """
    Fill a dataframe with powerboards that don't exist in it.
    """
    allpbs=set(range(0,10))
    pbs=set(df.pb.unique())
    misspbs=allpbs-pbs
    return df.append([{'pb': pb} for pb in misspbs]).sort_values('pb')

def pivot_ven_dataframe(df, oncol, invert=False):
    """
    Rotates a dataframe from a VEnParser into a table
    format.

    df: Original dataframe
    oncol: Name of column indicating on/off state
    invert: oncol==0 means ON
    """
    df=df.copy()
    str0 = "OFF" if not invert else "ON"
    str1 = "ON"  if not invert else "OFF"
        
    df['suffix']=df.apply(lambda x: str1 if x[oncol] else str0, axis=1, result_type='reduce')
    df=df.set_index(['pb','suffix']).unstack(-1)
    df.columns=df.columns.map(''.join)
    return df.reset_index()

class PBv3TableGen:
    def __init__(self, tableid):
        self.tableid=tableid

        self.coldef  =[{'name':['','Powerboard'],'id':'pb'}]
        self.condform=[{'if':{'column_id':'pb'},'width':'50px'}]        

    def add_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            if type(column["name"])!=tuple:
                name=('',column["name"])
            else:
                name=column['name']
                

            self.coldef+=[
                {'name':name,
                     'id':f'value_{column["id"]}',
                     'type':'numeric',
                     'format': Format(
                         precision=column.get('precision',2),
                         scheme=column.get('scheme',Scheme.fixed))
                }
            ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 1'
                        },
                        'backgroundColor': 'salmon'
                }
                ]

    def add_onoff_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            
            self.coldef+=[
                {'name':[f'{column["name"]} [{column.get("units","V")}]','OFF'],
                     'id':f'value_{column["id"]}OFF',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                },
                {'name':[f'{column["name"]} [{column.get("units","V")}]','ON'],
                     'id':f'value_{column["id"]}ON',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                }
                ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                ]

    def generate(self):
        return dash_table.DataTable(
            id=self.tableid,
            merge_duplicate_headers=True,
            columns=self.coldef,
            style_data_conditional=self.condform,
            style_header={'fontWeight': 'bold'},
            style_cell={'width':'85px'}
            )            

class TestingPage:
    def __init__(self, cfg, app):
        #
        # Run stuff
        self.cfg=cfg

        self.panels=[]
        panellabels=[]
        for panelcfg in cfg.panels:
            panelcfg=panelcfg.copy()
            name  =panelcfg.pop('name'  )
            runner=panelcfg.pop('runner')

            panelobj=getattr(panel,runner)(**panelcfg)

            self.panels.append(panelobj)
            panellabels.append(name)

        #
        # Layout related stuff
        pbchecks=[
            dcc.Checklist(id=f'pb-{pb}',
                          options = [{'label': 'Board {}'.format(pb), 'value': pb}])
            for pb in range(10)]

        runradios=[
            dcc.RadioItems(id='debug', options = [
                {'label': 'None'       , 'value': 0},
                {'label': 'Debug'      , 'value': 1},
                {'label': 'Super Debug', 'value': 4}
                ], value=0)
            ]

        # Tables
        table_ven  = PBv3TableGen('LVEn-table')
        table_ven.add_columns([
            {'id':'PADID','name':'Pad ID','precision':0},
            {'id':'RELIABILITY','name':'BER','precision':2},
            ])
        table_ven.add_onoff_columns([
            'linPOLV',
            {'id':'VOUT','name':'DC/DC Out'},
            {'id':'HVIIN'    , 'name':'HV In Current' ,'units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'HVIOUT'   , 'name':'HV Out Current','units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'AMACHVRET', 'name':'HVret'         ,'units':'counts','precision':0},            
            ])

        table_tio1 = PBv3TableGen('tio-table-1')
        table_tio1.add_onoff_columns(['OFout', 'CALx', 'CALy', 'Shuntx', 'Shunty'])

        table_tio2 = PBv3TableGen('tio-table-2')
        table_tio2.add_onoff_columns(['LDx0EN', 'LDx1EN', 'LDx2EN', 'LDy0EN', 'LDy1EN', 'LDy2EN'])

        table_temp = PBv3TableGen('temp-table')
        table_temp.add_columns([
            {'id':'NTCx' ,'name':'NTCx [cnt]' ,'precision':0},
            {'id':'NTCy' ,'name':'NTCy [cnt]' ,'precision':0},
            {'id':'NTCpb','name':'NTCpb [cnt]','precision':0},
            {'id':'CTAT' ,'name':'CTAT [cnt]' ,'precision':0},
            {'id':'PTAT' ,'name':'PTAT [cnt]' ,'precision':0},
        ])

        table_dcdcadj = PBv3TableGen('dcdcadj-table')
        table_dcdcadj.add_columns([
            {'id':'minusThirteen' ,'name':'-13%','precision':1},
            {'id':'minusSix'      ,'name':'-6%' ,'precision':1},
            {'id':'plusSix'       ,'name':'+6%' ,'precision':1},
        ])
        
        table_dcdc = PBv3TableGen('eff-table')
        table_dcdc.add_columns([
            {'id':'EFF2A','name':('2A Load','Efficiency'),'precision':2},
            {'id':'AMACPTAT2A','name':('2A Load','AMAC NTC PB [counts]'),'precision':0}
            ])

        tables=[
            table_ven    .generate(),
            table_tio1   .generate(),
            table_tio2   .generate(),
            table_temp   .generate(),
            table_dcdcadj.generate(),
            table_dcdc   .generate()
            ]

        # Create the layout
        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'testing.html', replace=pbchecks+runradios+tables, panels=panellabels)

        #
        # Callbacks

        ## Server-side Callbacks
        app.callback(
            [
                Output('panel-dbresult', 'data'),
                Output('auto-fill-msg', 'children'),
            ],
            Input('panel-populate-button', 'n_clicks'),
            [
                State('panel-number', 'value'),
                State('itkdb_auth', 'data')
            ],
            prevent_initial_call=True
            )(self.populateFromDB)
 
        app.callback(
            Output('upload-test-msg', 'children'),
            [Input('panel-upload-test-result-button', 'n_clicks')],
            [
                State('itkdb_auth', 'data'),
                State('tabs', 'value'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.uploadTestResults)
 
        app.callback(
            Output('upload-status-msg', 'children'),
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
        )(self.updateUploadTestResultsStatus)
      
        app.callback(
            Output('active-test-msg', 'children'),
            [Input('active-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
            ]
            )(self.runStandBy)


        app.callback(
            Output('basic-test-msg', 'children'),
            [Input('basic-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.runBasicTests)

        app.callback(
            Output('advanced-test-msg', 'children'),
            [Input('advanced-test-button', 'n_clicks')],
            [
                State('tabs', 'value'),
                State('debug', 'value'),
                State('panel-number', 'value'),
                State('version', 'value'),
                State('batch', 'value'),
                State('pb-0', 'value'),
                State('pb-1', 'value'),
                State('pb-2', 'value'),
                State('pb-3', 'value'),
                State('pb-4', 'value'),
                State('pb-5', 'value'),
                State('pb-6', 'value'),
                State('pb-7', 'value'),
                State('pb-8', 'value'),
                State('pb-9', 'value'),
                State('serial-0', 'value'),
                State('serial-1', 'value'),
                State('serial-2', 'value'),
                State('serial-3', 'value'),
                State('serial-4', 'value'),
                State('serial-5', 'value'),
                State('serial-6', 'value'),
                State('serial-7', 'value'),
                State('serial-8', 'value'),
                State('serial-9', 'value')
            ]
            )(self.runAllTests)        

        app.callback(
            Output('stop-msg', 'children'),
            [Input('stop-button', 'n_clicks')],
            [
                State('tabs', 'value'),
            ]
            )(self.stopRun)        
        
        app.callback(
            [
                Output('display-test', 'children'),
            ],
            [Input('tabs','value'), Input('interval-component-run', 'n_intervals')]
        )(self.updateRunData)
        
        app.callback(
            Output('active-result-msg', 'children'),
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
        )(self.updateStandByData)

        app.callback(
            [
                Output('LVEn-table', 'data'),
                Output('ven-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('ven-last-update', 'data')]
        )(self.updateVEndata)
        
        app.callback(
            [
                Output('tio-data', 'data'),
                Output('tio-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('tio-last-update', 'data')]
        )(self.updateTIOData)

        app.callback(
            [
                Output('temp-table', 'data'),
                Output('temp-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('temp-last-update', 'data')]
        )(self.updateTempData)

        app.callback(
            [
                Output('dcdcadj-table', 'data'),
                Output('dcdcadj-last-update', 'data')
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dcdcadj-last-update', 'data')]
        )(self.updateDCDCAdjData)        

        app.callback(
            [
                Output('lviv-data', 'data'),
                Output('lviv-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('lviv-last-update', 'data')]
        )(self.updateLVIVdata)

        app.callback(
            [
                Output('amsl-data', 'data'),
                Output('amsl-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('amsl-last-update', 'data')]
        )(self.updateAMSlopedata)

        app.callback(
            [
                Output('dac-data', 'data'),
                Output('dac-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dac-last-update', 'data')]
        )(self.updateDACdata)

        app.callback(
            [
                Output('eff-table', 'data'),
                Output('dcdc-data', 'data'),
                Output('dcdc-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('dcdc-last-update', 'data')]
        )(self.updateDCDCdata)

        app.callback(
            [
                Output('hvrt-data', 'data'),
                Output('hvrt-last-update', 'data'),
            ],
            [Input('interval-component-run', 'n_intervals'), Input('tabs','value')],
            [State('hvrt-last-update', 'data')]
        )(self.updateHVdata)

        app.callback(
            Output('panel-status', 'data'),
            [Input('interval-component-status', 'n_intervals')]
        )(self.updateRunStatus)
        
        ## Client-side Callbacks

        # Store checkbox settings per panel
        panelstoreelements=['panel-number', 'version', 'batch']
        panelstoreelements+=[f'pb-{pb}' for pb in range(10)]
        panelstoreelements+=[f'serial-{pb}' for pb in range(10)]
        panelstoreelements+=['debug']

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='storePanelInfo'
                ),
            Output('panel-storage', 'data'),
            [Input(name, 'value') for name in panelstoreelements],
            [State('panel-storage', 'data'), State('tabs','value')])

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='restorePanelInfo'
                ),
            [Output(name, 'value') for name in panelstoreelements],
            [Input('tabs', 'value'), Input('panel-dbresult', 'data')],
            [State('panel-storage', 'data')])
        
        # Update status styles
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTabColors'
                ),
            [o for p in range(len(self.panels)) for o in [Output(f'tab-{p}', 'style'),Output(f'tab-{p}', 'selected_style')]],
            [Input('panel-status', 'data')],
            prevent_initial_call=True
            )

        # Graph updates
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTIOtable'
                ),
                [
                    Output('tio-table-1','data'),
                    Output('tio-table-2','data')
                ],
                [Input('tio-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateLVIVgraph'
                ),
                Output('lviv-graph','figure'),
                [Input('lviv-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateAMSlopegraph'
                ),
                Output('amsl-graph','figure'),
                [Input('amsl-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDACgraph'
                ),
                [
                    Output('shuntx-graph' ,'figure'),
                    Output('shunty-graph','figure'),
                    Output('calx-graph','figure'),
                    Output('caly-graph','figure')
                ],
                [Input('dac-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDCDCgraph'
                ),
                [
                    Output('eff-graph' ,'figure'),
                    Output('temp-graph','figure'),
                    Output('icur-graph','figure'),
                    Output('ocur-graph','figure')
                ],
                [Input('dcdc-data', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateHVretgraph'
                ),
                [
                    Output('gain0-graph','figure'),
                    Output('gain1-graph','figure'),
                    Output('gain2-graph','figure'),
                    Output('gain4-graph','figure')
                ],
                [Input('hvrt-data', 'data')]
            )

    def populateFromDB(self, button, panel, user):
        """
        Query the database for Powerboard ID's on a panel and return
        the values in a dictionary.

        If an error occurs (invalid login, mixing version/batch numbers),
        then an exception is thrown.

        Dictionary Keys:
         - version: Version number
         - batch: Batch number
         - pbEnable: List of 10 booleans, true if the Powerboard is preset
         - pbNumber: List of 10 integers, corresponding to Powerboard number
        """
        if panel == '':
            return None, ' Please provide a panel number'
        if user==None:
            return None, ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if not user.is_authenticated():
            return None, ' Please login to ITk DB first!!!'
        if user.is_expired():
            return None, ' ITk DB login expired, please login again!!!'

        client=itkdb.Client(user=user)

        #
        # Query the database
        result={'pbEnable':[False]*10, 'pbNumber':[0]*10}
        carrier=pwbdb.Component(f'20USBPC{panel}', client)
        componentType = ""
        try:
            componentType = carrier.componentType
        except:
            componentType = "InValid"
        if componentType != "PWB_CARRIER":
            return None, ' Panel not found in DB. Please provide a valid panel number!!!'

        for pbNum,pb in carrier.children.get('PWB',{}).items():
            # Update common values
            if result.get('version', pb.property('VERSION'))!=pb.property('VERSION'):
                return None, ' Cannot mix version numbers'
            result['version']=pb.property('VERSION')

            if result.get('batch', pb.property('BATCH'))!=pb.property('BATCH'):
                return None, ' Cannot mix batch numbers'
            result['batch']=pb.property('BATCH')

            # Enable powerboard
            result['pbEnable'][pbNum]=True
            result['pbNumber'][pbNum]=pb.property('PB_NUMBWE')

        return result, ' '

    def uploadTestResults(self, button, user, tab, *args):
        """
        Upload test results to idkdb
        """
        if button==None:
            raise PreventUpdate

        panel  =args[0].strip()
        if panel == '':
            return ' Please provide a panel number!'

        if args[1] == '':
            return ' Please provide a powerboard version number!'
        version=int(args[1])

        if args[2] == '':
            return ' Please provide a powerboard batch number!'
        batch  =int(args[2])

        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if len(boards)==0:
            return 'No boards selected!'

        if user==None:
            return ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'
        if user.is_expired():
            return ' ITk DB login expired, please login again!!!'

        client=itkdb.Client(user=user)

        msg = ""
        try:
            msg = self.panels[int(tab)].runUploadData(client, version, batch, boards, serials, panelName=panel)
        except:
            return sys.exc_info()[0]
        
        return msg

    def runStandBy(self, button, tab, debug, *args):
        """
        Run only the basic set of tests.
        """
        if button==None:
            raise PreventUpdate

        if self.panels[int(tab)].running:
            return 'Test program already running!'

        try:
            self.panels[int(tab)].runGeneral(program='pbv3_stand_by', option=None, useConfig=True)
        except:
            return sys.exc_info()[0]

        return ''

    def runBasicTests(self, button, tab, debug, *args):
        """
        Run only the basic set of tests.
        """
        if button==None:
            raise PreventUpdate

        panel  =args[0].strip()
        version=int(args[1])
        batch  =int(args[2])
        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if self.panels[int(tab)].running:
            return 'Test program already running!'

        if panel=='':
            return 'Must specify a panel name!'

        if len(boards)==0:
            return 'No boards selected'

        try:
            self.panels[int(tab)].run(version, batch, boards, serials, panelName=panel, institution=self.cfg.institution, debug=debug, basic=True)
        except:
            return sys.exc_info()[0]

        return ''

    def runAllTests(self, button, tab, debug, *args):
        """
        Run the full suit of tests.
        """
        if button==None:
            raise PreventUpdate

        panel  =args[0].strip()
        version=args[1].strip()
        batch  =args[2].strip()
        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if self.panels[int(tab)].running:
            return 'Test program already running!'

        if panel=='':
            return 'Must specify a panel name!'

        if len(boards)==0:
            return 'No boards selected'

        self.panels[int(tab)].run(version, batch, boards, serials, panelName=panel, institution=self.cfg.institution, debug=debug, basic=False)

        return ''

    def stopRun(self, button, tab):
        """
        Stop any running tests
        """
        if button==None:
            raise PreventUpdate

        if not self.panels[int(tab)].running:
            return 'Test program not running!'

        self.panels[int(tab)].stop()

        return ''
    
    def updateRunStatus(self, interval):
        return [panel.running for panel in self.panels]

    def updateRunData(self, tab, interval):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        terminal = '\n'.join(panel.data)

        return [terminal]

    def updateTempData(self, interval, tab, lastUpdate):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]        
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                           and lastUpdate['timestamp']>=panel.temp.lastUpdate
                                           ): # no new data
            raise PreventUpdate

        lastUpdate=max([panel.temp.lastUpdate])

        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # Temperature
        tbl=tbl.merge(panel.temp.data, on='pb', how='outer')

        # Final table
        tbl=tbl.to_dict('records')

        return tbl, {'tab': tab, 'timestamp': lastUpdate}

    def updateDCDCAdjData(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]        
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                           and lastUpdate['timestamp']>=panel.dcad.lastUpdate
                                           ): # no new data
            raise PreventUpdate

        lastUpdate=max([panel.dcad.lastUpdate])

        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # DCDC Adjust        
        tbl=tbl.merge(panel.dcad.data, on='pb', how='outer')

        # Turn into %
        for column in tbl.columns:
            if not column.startswith('value'):
                continue
            tbl[column]=tbl[column]*100

        # Final table
        tbl=tbl.to_dict('records')

        return tbl, {'tab': tab, 'timestamp': lastUpdate}

    def updateStandByData(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        post_msg = ''
        panel_str = ''.join(panel.data)
        #clear message once main tests started
        if "Performing PS and active board checks" not in panel_str:
            return ''
        else:
            post_msg = 'Waiting for PS and I2C check result... '
        if "PSINIT passed" in panel_str:
            post_msg = 'PS initialization OK. '
        if "PSINIT failed" in panel_str:
            post_msg = 'PS init FAILED!! '
        if "I2CDEV passed" in panel_str:
            post_msg += ' I2C check OK. '
        if "I2CDEV failed" in panel_str:
            post_msg += ' I2C check FAILED!! '
        if "PS initialization OK" in post_msg and "I2C check OK" in post_msg:
            post_msg += ' System is ready for tests. '

        return post_msg

    def updateUploadTestResultsStatus(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        # Terminal output
        post_msg = ''

        if len(panel.dbUploadString.contents) > 0:
            post_msg = 'Upload finished for '+str(len(panel.dbUploadString.contents)) + ' PBs ('+str(panel.dbUploadString.nContents)+' files).'
            if panel.dbUploadString.hasError:
                post_msg += ' Some uploads failed, see terminal output.'
            if panel.dbUploadString.hasWarning:
                post_msg += ' Some uploads have warning, see terminal output.'

        return post_msg


    def updateVEndata(self, interval, tab, lastUpdate):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]        
        panel.parse()

        lastUpdatePanel=max([panel.padi.lastUpdate, panel.bert.lastUpdate, panel.lven.lastUpdate, panel.hven.lastUpdate])
        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                       and lastUpdate['timestamp']>=lastUpdatePanel): # no new data
            raise PreventUpdate

        
        #
        # Random voltage on/off and other checks
        ven = pd.DataFrame(data={'pb':range(10)})        

        # Pad ID
        ven=ven.merge(panel.padi.data, on='pb', how='outer')

        # BER
        ven=ven.merge(panel.bert.data, on='pb', how='outer')

        # Reformat LV_ENABLE
        if len(panel.lven.data.index)>0:
            dlven=pivot_ven_dataframe(panel.lven.data, 'DCDCen')
            ven=ven.merge(dlven, on='pb', how='outer')

        # Reformat HV_ENABLE
        if len(panel.hven.data.index)>0:
            dhven=pivot_ven_dataframe(panel.hven.data, 'CntSetHV0en')
            ven=ven.merge(dhven, on='pb', how='outer')

        # Reformat OFin test
        if len(panel.ofin.data.index)>0:
            dofin=pivot_ven_dataframe(panel.ofin.data, 'OF', True)
            ven=ven.merge(dofin, on='pb', how='outer')

        # Final table
        ven=ven.to_dict('records')

        return ven, {'tab': tab, 'timestamp': lastUpdatePanel}
    
    def updateTIOData(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.tout.lastUpdate): # no new data
            raise PreventUpdate


        # Toggle output tables
        tio=pd.DataFrame(data={'pb':range(10)})

        tout=panel.tout.data
        if len(tout.index)>0:
            tout['name']=tout[['output','state']].apply(lambda x: x['output']+('ON' if x['state'] else 'OFF'), axis=1, result_type='reduce')
            tout=tout.set_index(['pb','name'])[['error','value']].unstack(-1)
            tout.columns=tout.columns.map('_'.join)

        tio=tio.merge(tout,on='pb',how='outer')
        tio=tio.to_dict('records')

        return tio, {'tab': tab, 'timestamp': panel.tout.lastUpdate}

    def updateLVIVdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.lviv.lastUpdate): # no new data
            raise PreventUpdate

        # Prepare data
        p_lviv=panel.lviv.data.pb.unique()
        d_lviv=[]
        for pb in range(0,10):
            d_lviv.append({
                'VIN'       : panel.lviv.data[panel.lviv.data.pb==pb].VIN        if pb in p_lviv else [],
                'AMACDCDCIN': panel.lviv.data[panel.lviv.data.pb==pb].AMACDCDCIN if pb in p_lviv else []
                })

        return d_lviv, {'tab': tab, 'timestamp': panel.lviv.lastUpdate}

    def updateAMSlopedata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.amsl.lastUpdate): # no new data
            raise PreventUpdate

        # Prepare data
        p_amsl=panel.amsl.data.pb.unique()
        d_amsl=[]
        for pb in range(0,10):
            d_amsl.append({
                'CALIN'  :panel.amsl.data[panel.amsl.data.pb==pb].CALIN   if pb in p_amsl else [],
                'AMACCAL':panel.amsl.data[panel.amsl.data.pb==pb].AMACCAL if pb in p_amsl else []
                })

        return d_amsl, {'tab': tab, 'timestamp': panel.amsl.lastUpdate}

    def updateDACdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        lastUpdatePanel=max([panel.rshx.lastUpdate, panel.rshy.lastUpdate, panel.rcax.lastUpdate, panel.rcay.lastUpdate])
        if type(lastUpdate)==dict and (lastUpdate['tab']==tab
                                       and lastUpdate['timestamp']>=lastUpdatePanel
                                       ): # no new data
            raise PreventUpdate

        #
        # Empty dataframe with data
        p_rshx=panel.rshx.data.pb.unique()
        p_rshy=panel.rshy.data.pb.unique()
        p_rcax=panel.rcax.data.pb.unique()
        p_rcay=panel.rcay.data.pb.unique()
        d_ramp={'rshx':[], 'rshy':[], 'rcax':[], 'rcay':[]}
        for pb in range(0,10):
            pbdata=panel.rshx.data[panel.rshx.data.pb==pb]
            d_ramp['rshx'].append({col:pbdata[col] if pb in p_rshx else [] for col in pbdata.columns})

            pbdata=panel.rshy.data[panel.rshy.data.pb==pb]
            d_ramp['rshy'].append({col:pbdata[col] if pb in p_rshy else [] for col in pbdata.columns})

            pbdata=panel.rcax.data[panel.rcax.data.pb==pb]
            d_ramp['rcax'].append({col:pbdata[col] if pb in p_rcax else [] for col in pbdata.columns})

            pbdata=panel.rcay.data[panel.rcay.data.pb==pb]
            d_ramp['rcay'].append({col:pbdata[col] if pb in p_rcay else [] for col in pbdata.columns})

        return d_ramp, {'tab': tab, 'timestamp': lastUpdatePanel}

    def updateDCDCdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.dcdc.lastUpdate): # no new data        
            raise PreventUpdate

        # Graph data
        p_dcdc=panel.dcdc.data.pb.unique()
        d_dcdc=[]
        for pb in range(0,10):
            d_dcdc.append({
                'IOUT'      :panel.dcdc.data[panel.dcdc.data.pb==pb].IOUT       if pb in p_dcdc else [],
                'EFFICIENCY':panel.dcdc.data[panel.dcdc.data.pb==pb].EFFICIENCY if pb in p_dcdc else [],
                'AMACPTAT'  :panel.dcdc.data[panel.dcdc.data.pb==pb].AMACPTAT   if pb in p_dcdc else [],
                'AMACCUR10V':panel.dcdc.data[panel.dcdc.data.pb==pb].AMACCUR10V if pb in p_dcdc else [],
                'AMACCUR1V' :panel.dcdc.data[panel.dcdc.data.pb==pb].AMACCUR1V  if pb in p_dcdc else []
                })

        # DCDC efficiency at 2A
        dcdc2=panel.dcdc.data[panel.dcdc.data.IOUTSET==2].rename(columns={
            'EFFICIENCY':'value_EFF2A',
            'AMACPTAT':'value_AMACPTAT2A',
        })
        dcdc2['error_EFF2A']=dcdc2.value_EFF2A<0.6

        dcdc2=pad_dataframe_pbs(dcdc2)
        t_dcdc=dcdc2.to_dict('records')
        
        return t_dcdc, d_dcdc, {'tab': tab, 'timestamp': panel.dcdc.lastUpdate}

    def updateHVdata(self, interval, tab, lastUpdate):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        if type(lastUpdate)==dict and (lastUpdate['tab']==tab and lastUpdate['timestamp']==panel.hvrt.lastUpdate): # no new data
            raise PreventUpdate

        #
        # Graphs
        p_gains=panel.hvrt.data.pb.unique()
        p_hvrt=panel.hvrt.data.pb.unique()
        d_hvrt=[]
        for pb in range(0,10):
            d_hvrt.append({
                'HVIOUT'   :panel.hvrt.data[panel.hvrt.data.pb==pb].HVIOUT    if pb in p_hvrt else [],
                'AMACGAIN0':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN0 if pb in p_hvrt else [],
                'AMACGAIN1':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN1 if pb in p_hvrt else [],
                'AMACGAIN2':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN2 if pb in p_hvrt else [],
                'AMACGAIN4':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN4 if pb in p_hvrt else [],
                'AMACGAIN8':panel.hvrt.data[panel.hvrt.data.pb==pb].AMACGAIN8 if pb in p_hvrt else []
                })

        return d_hvrt, {'tab': tab, 'timestamp': panel.hvrt.lastUpdate}
    
