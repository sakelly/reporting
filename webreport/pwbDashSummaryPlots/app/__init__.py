import os, sys
from os import path
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go
import json
import pandas as pd
import urllib.parse
import subprocess
import argparse

testResults = {}
testResults_v3a = {}
testResults_v3b = {}

reportingDir = os.getcwd()

with open(reportingDir + '/webreport/pwbDashSummaryPlots/config.json', 'r') as f:
    config = json.load(f)

def fillData(pathName, testResults, testResults_v3a, testResults_v3b):
    if path.exists(pathName + '/testResults.json'):
        with open(pathName + '/testResults.json', 'r') as f:
            try:
                testResults.update(json.load(f))
            except ValueError:
                pass
        
    if path.exists(pathName + '/testResults_v3a.json'):
        with open(pathName + '/testResults_v3a.json', 'r') as f:
            try:
                testResults_v3a.update(json.load(f))
            except ValueError:
                pass
     
    if path.exists(pathName + '/testResults_v3b.json'):
        with open(pathName + '/testResults_v3b.json', 'r') as f:
            try:
                testResults_v3b.update(json.load(f))
            except ValueError:
                pass

def getValueByPosition(versionResults, test, subtest, position):
    values = []
    for i in versionResults[test][subtest]:
        values.append(i[position])
    return values

fillData(config.get('saveLocation', 'webreport/pwbDashSummaryPlots/data'), testResults, testResults_v3a, testResults_v3b)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, requests_pathname_prefix=config.get('requests_pathname_prefix', '/'), external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.Div([
    html.Div(
        id='test-type-caption',
        children="Test Type:"
    ),
    dcc.Dropdown(
        id='test-type',
        options=[{'label': i, 'value': i} for i in testResults.keys()] if len(testResults)>0 else [],
        value='DCDCEFFICIENCY' if len(testResults)>0 else ''
    ),
    html.Div(
        id='subtest-type-caption',
        children="Plot variable:"
    ),
    dcc.Dropdown(
        id='subtest-type',
        options=[{'label': i, 'value': i} for i in testResults['DCDCEFFICIENCY'].keys()] if len(testResults)>0 else [],
        value='EFFICIENCY' if len(testResults)>0 else ''
    ),
    html.Div(
        id='subtest-compare-caption',
        children="Filter variable:"
    ),
    dcc.Dropdown(
        id='subtest-compare',
        options=[{'label': i, 'value': i} for i in testResults['DCDCEFFICIENCY'].keys()] if len(testResults)>0 else [],
        value='IOUT' if len(testResults)>0 else ''
    ),
    html.Div(
        id='subtest-compare-values-caption',
        children="Filter variable value:"
    ),
    dcc.Dropdown(
        id='subtest-compare-values',
        options=[{'label': str(i), 'value': index} for index, i in enumerate(testResults['DCDCEFFICIENCY']['IOUT'][0])] if len(testResults)>0 else [],
        value=0 if len(testResults)>0 else ''
    ),
    html.Button('Generate Graph', id='graph-button', n_clicks=0),
    dcc.RadioItems(
        id='histnorm-type',
        options=[{'label': i, 'value': i} for i in ['Percentage', 'Counts']],
        value='Counts',
        labelStyle={'display': 'inline-block'}
    ),
    html.Div(children=[
        dcc.Graph(
            id='test-graph'
        )],
        style={'height':1000, 'width':1000}
    ),
    html.A(
        'Download data as csv file',
        id='download-link',
        download="empty.csv",
        href="",
        target="_blank"
    )
    ])
])

@app.callback(
    [dash.dependencies.Output('subtest-type', 'options'),
     dash.dependencies.Output('subtest-compare', 'options')],
    [dash.dependencies.Input('test-type', 'value')])
def update_subtests(testType):
    if testType in testResults:
        subtest_type=[{'label': i, 'value': i} for i in testResults[testType]]
        subtest_compare=[{'label': i, 'value': i} for i in testResults[testType]]
    else:
        subtest_type=[]
        subtest_compare=[]
    return subtest_type, subtest_compare

@app.callback(
    dash.dependencies.Output('subtest-type', 'value'),
    [dash.dependencies.Input('subtest-type', 'options')])
def update_subtest(subtestType):
    if not bool(testResults):
        return ''
    else:
        return subtestType[0]['value']

@app.callback(
    dash.dependencies.Output('subtest-compare', 'value'),
    [dash.dependencies.Input('subtest-compare', 'options')])
def update_subtest_compare(subtestCompare):
    if not bool(testResults):
        return ''
    else:
        return subtestCompare[0]['value']

@app.callback(
    dash.dependencies.Output('subtest-compare-values', 'value'),
    [dash.dependencies.Input('subtest-compare-values', 'options')])
def update_subtest_compare_values(subtestCompareValues):
    return 0

@app.callback(
    dash.dependencies.Output('subtest-compare-values', 'options'),
    [dash.dependencies.Input('subtest-compare', 'value')],
    [dash.dependencies.State('test-type', 'value')])
def subtest_compare_values(subtestCompare, testType):
    if not bool(testResults):
        return []
    else:
        return [{'label': str(i), 'value': index} for index, i in enumerate(testResults[testType][subtestCompare][0])]

@app.callback(
    [dash.dependencies.Output('test-graph', 'figure'),
     dash.dependencies.Output('download-link', 'href'),
     dash.dependencies.Output('download-link', 'download')],
    [dash.dependencies.Input('graph-button', 'n_clicks'),
    dash.dependencies.Input('histnorm-type', 'value')],
    [dash.dependencies.State('test-type', 'value'),
     dash.dependencies.State('subtest-type', 'value'),
     dash.dependencies.State('subtest-compare', 'value'),
     dash.dependencies.State('subtest-compare-values', 'value'),
     dash.dependencies.State('subtest-compare-values', 'options')])
def update_graph(button, graphType, testType, subtestType, subtestCompare, position, subtestCompareValues):
    fig = go.Figure()
    fig.update_layout(barmode='overlay',
        autosize=False,
        width=1000,
        height=700)

    if(button!=0):
        x = getValueByPosition(testResults, testType, subtestType, position)
        x_v3a = getValueByPosition(testResults_v3a, testType, subtestType, position)
        x_v3b = getValueByPosition(testResults_v3b, testType, subtestType, position)

        label = subtestCompareValues[position]['label']

        df = pd.DataFrame(data={"all_versions": pd.Series(x), "version_3a": pd.Series(x_v3a), "version_3b": pd.Series(x_v3b)})
        csv_string = df.to_csv(index=False, encoding='utf-8')
        csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)

        fig.add_trace(go.Histogram(x=x, name='All Versions, Total: ' + str(len(x)), histnorm = 'percent' if graphType == 'Percentage' else ''))
        fig.add_trace(go.Histogram(x=x_v3a, name='Version 3a, Total: ' + str(len(x_v3a)), histnorm = 'percent' if graphType == 'Percentage' else ''))
        fig.add_trace(go.Histogram(x=x_v3b, name='Version 3b, Total: ' + str(len(x_v3b)), histnorm = 'percent' if graphType == 'Percentage' else ''))

        fig.update_traces(opacity=0.60)

        return fig, csv_string, testType + "_" + subtestType + "_" + subtestCompare + "_" + label + ".csv"

    else:
        df = pd.DataFrame(data={})
        csv_string = df.to_csv(index=False, encoding='utf-8')
        csv_string = "data:text/csv;charset=utf-8," + urllib.parse.quote(csv_string)

        return fig, csv_string, "empty.csv"
